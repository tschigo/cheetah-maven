#Cheetah


##Installationsanleitung


* Clone git repository

		git clone https://bitbucket.org/tschigo/cheetah-maven.git -b beta
		
* Datenbank generierung
		
		mysql -u username -p cheetah < cheetah-maven/src/main/resources/cheetah_2014-11-06.sql


* import in Eclipse oder IntelliJ als maven Projekt oder von der Kommandozeile (mvn)

* hibernate.cfg.xml file editieren (src/main/resources/)

		<property name="hibernate.connection.driver_class"> JDBC DRIVER CLASS </property>
		<property name="hibernate.connection.password"> PASSWORD </property>
		<property name="hibernate.connection.url"> JDBC DATABASE URL </property>
		<property name="hibernate.connection.username"> USER </property>

* Eclipse: 
	* File -> Export…
	* Web/WAR file -> Next >
	* Web project: cheetah-maven
	* Destination: <destination>
	* Target runtime: Apache Tomcat v7.0
	* Finish
	
* Verbindung zu Tomcat7-Server aufbauen (ssh/scp,ftp)
* Kopiere WAR file nach “TOMCAT_DIR/webapps/cheetah.war”
* Starte Tomcat Server -> cheetah.war file wird automatisch deployed
* Aufrufbar unter hostname:port/cheetah

## XML
Siehe cheetah-maven/src/main/resources/XML-Documentation.pdf