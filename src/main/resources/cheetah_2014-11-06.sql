# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.38)
# Database: cheetah
# Generation Time: 2014-11-06 17:53:22 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP DATABASE IF EXISTS `cheetah`;
CREATE DATABASE `cheetah`;
USE `cheetah`;

# Dump of table Answer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Answer`;

CREATE TABLE `Answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` varchar(4000) DEFAULT NULL,
  `issue_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1hrco6fakqms6gub5i27br8r4` (`issue_id`),
  KEY `FK_ctpnqpc7glahp2777dqgj5chr` (`user_id`),
  CONSTRAINT `FK_1hrco6fakqms6gub5i27br8r4` FOREIGN KEY (`issue_id`) REFERENCES `Issue` (`id`),
  CONSTRAINT `FK_ctpnqpc7glahp2777dqgj5chr` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Config`;

CREATE TABLE `Config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cooldown` int(11) DEFAULT NULL,
  `expiringdate` datetime DEFAULT NULL,
  `format` varchar(255) NOT NULL,
  `releasedate` datetime DEFAULT NULL,
  `template` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Image
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Image`;

CREATE TABLE `Image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `height` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table InputField
# ------------------------------------------------------------

DROP TABLE IF EXISTS `InputField`;

CREATE TABLE `InputField` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `max` int(11) NOT NULL,
  `maxlength` int(11) NOT NULL,
  `min` int(11) NOT NULL,
  `minlength` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Issue
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Issue`;

CREATE TABLE `Issue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dependency` varchar(255) DEFAULT NULL,
  `hint` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `required` bit(1) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `inputField_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sn0g8orunkb765e3b5jrujblo` (`image_id`),
  KEY `FK_qypfdrjflkliitoveljd553ep` (`inputField_id`),
  KEY `FK_kov6ubdgr5dpo3tvltrsbo9uo` (`page_id`),
  CONSTRAINT `FK_kov6ubdgr5dpo3tvltrsbo9uo` FOREIGN KEY (`page_id`) REFERENCES `Page` (`id`),
  CONSTRAINT `FK_qypfdrjflkliitoveljd553ep` FOREIGN KEY (`inputField_id`) REFERENCES `InputField` (`id`),
  CONSTRAINT `FK_sn0g8orunkb765e3b5jrujblo` FOREIGN KEY (`image_id`) REFERENCES `Image` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table OptionField
# ------------------------------------------------------------

DROP TABLE IF EXISTS `OptionField`;

CREATE TABLE `OptionField` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `inputField_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7ng0dtvgk5neamu1h7f7cypf0` (`inputField_id`),
  CONSTRAINT `FK_7ng0dtvgk5neamu1h7f7cypf0` FOREIGN KEY (`inputField_id`) REFERENCES `InputField` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Page
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Page`;

CREATE TABLE `Page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pagenumber` int(11) DEFAULT NULL,
  `survey_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_jyfbqq36d9w7kahmmafefuby6` (`survey_id`),
  CONSTRAINT `FK_jyfbqq36d9w7kahmmafefuby6` FOREIGN KEY (`survey_id`) REFERENCES `Survey` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table PageTracking
# ------------------------------------------------------------

DROP TABLE IF EXISTS `PageTracking`;

CREATE TABLE `PageTracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mousePos` text,
  `scroll` text,
  `time` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t5tf4cfkwx34dvlnoutpl6frt` (`page_id`),
  KEY `FK_oidxood5tikybods43gwmxd2i` (`user_id`),
  CONSTRAINT `FK_oidxood5tikybods43gwmxd2i` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`),
  CONSTRAINT `FK_t5tf4cfkwx34dvlnoutpl6frt` FOREIGN KEY (`page_id`) REFERENCES `Page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Survey
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Survey`;

CREATE TABLE `Survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `config_id` int(11) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_eddig9my7nnavsfmqjtab1wrx` (`title`),
  KEY `FK_7b9ktqucwyar9kpmg8uak05fd` (`config_id`),
  CONSTRAINT `FK_7b9ktqucwyar9kpmg8uak05fd` FOREIGN KEY (`config_id`) REFERENCES `Config` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table Tracking
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Tracking`;

CREATE TABLE `Tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `browser` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `resolution` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table User
# ------------------------------------------------------------

DROP TABLE IF EXISTS `User`;

CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creationDate` datetime DEFAULT NULL,
  `sessionID` varchar(255) DEFAULT NULL,
  `survey_id` int(11) DEFAULT NULL,
  `tracking_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8sjas4l558bqa1qhpw02gl5e6` (`survey_id`),
  KEY `FK_9qk7c7tfl4r06r0nayuyp5pdd` (`tracking_id`),
  CONSTRAINT `FK_8sjas4l558bqa1qhpw02gl5e6` FOREIGN KEY (`survey_id`) REFERENCES `Survey` (`id`),
  CONSTRAINT `FK_9qk7c7tfl4r06r0nayuyp5pdd` FOREIGN KEY (`tracking_id`) REFERENCES `Tracking` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
