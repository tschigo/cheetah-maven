
function getMousePosition() {
	$(document).one(
		"mousemove",
		function(event) {
			$('#logM').text('[' + event.pageX + '/' + event.pageY + ']');
			$('#mouse').val($('#mouse').val() + '[' + event.pageX + '/'	+ event.pageY + '],');
			setTimeout(function() {	getMousePosition();	}, 1000);
		}
	);
}

function getScrollPosition() {
	$(document).one(
		"scroll",
		function(event) {
			$('#log').text('[' + $(document).scrollTop() + '/'	+ $(document).scrollLeft() + ']');
			$('#scroll').val($('#scroll').val() + '[' + $(document).scrollTop() + '/' + $(document).scrollLeft() + '],');
			setTimeout(function() { getScrollPosition() }, 500);
		}
	);
}

function waitForWhichBrowser(cb) {
	var callback = cb;
	function wait() {
		if (typeof WhichBrowser == 'undefined'){
			window.setTimeout(wait, 100);
		} else{
			callback();
		}
	}
	wait();
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$.getJSON("http://www.telize.com/geoip?callback=?", function(data) {
	$('#formular').append('<input type="hidden" name="ip" id="ip" value="' + data.ip + '" />');
});


