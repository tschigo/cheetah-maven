<%@ page import="org.hibernate.Session"%>
<%@ page import="at.ac.uibk.cheetah.hibernate.HibernateFactory"%>
<%@ page import="java.util.List"%>
<%@ page import="at.ac.uibk.cheetah.model.Survey"%>
<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cheetah - Admin</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/uibk.css">
    <link rel="shortcut icon" href="favicon.ico" />
    <style type="text/css">
        body{
            font-weight: normal;
            font-size: 13px;
        }
    </style>
</head>
<body>

	<%
		String userName = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("user"))
					userName = cookie.getValue();
			}
		}
		if (userName == null)
			response.sendRedirect("Admin");
	%>


	<!--[if lt IE 8]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
	<div id="header">
		<a href="http://uibk.ac.at" target="_blank">Uibk</a>
	</div>

	<div id="main">

		<a href="http://uibk.ac.at" target="_blank"><img id="logo" src="img/uibk.png" /></a>
        <hr />

        <h3>File Upload:</h3>
		<form action="UploadServlet" method="post" enctype="multipart/form-data">
			<input type="file" name="file" size="50" />
            <input type="submit" value="Upload File" />
		</form>

		<hr />

		<%
			String root = request.getServerName() + ":"
					+ request.getServerPort();
			Session sessionSurvey = HibernateFactory.create().openSession();

			sessionSurvey.beginTransaction();

			List<?> surveys = HibernateFactory.select(sessionSurvey,
					"FROM Survey WHERE active = 1");

			sessionSurvey.close();
			HibernateFactory.close();

			String surveyDropDown = "<select id=\"survey\" name=\"survey\">\n";
			String surveyURLs = "<ul>\n";
            String urlsTable = "<table border=\"1px\">\n";
            urlsTable += "<tr>\n";
            urlsTable += "<th>Link</th>\n";
            urlsTable += "<th>Test mode</th>\n";
            urlsTable += "<th>URL</th>\n";
            urlsTable += "</tr>\n";

			for (Object o : surveys) {
				surveyDropDown += "\t<option value=\""
						+ ((Survey) o).getTitle() + "\">"
						+ ((Survey) o).getTitle() + "</option>\n";

                urlsTable += "<tr>\n";
                urlsTable += "<td>" + "<a href=\"Page?survey=" + ((Survey) o).getTitle() + "\" target=\"_blank\">"	+ ((Survey) o).getTitle() + "</a> " + "</td>\n";
                urlsTable += "<td>" + "<a href=\"Page?mode=test&survey=" + ((Survey) o).getTitle() + "\" target=\"_blank\">"	+ ((Survey) o).getTitle() + "</a> " + "</td>\n";
                urlsTable += "<td>" + "<input size=\"70\" value=\"http://" + root + request.getContextPath() + "/Page?survey=" + ((Survey) o).getTitle() + "\"/>" + "</td>\n";
                urlsTable += "</tr>\n";

            }
            urlsTable += "</table>\n";
			surveyDropDown += "</select>\n";
			surveyURLs += "</ul>\n";
		%>

		<h3>db export</h3>
		<form action="DownloadServlet" method="get">
			<%=surveyDropDown%>
            <input type="checkbox" name="userTracking" value="true"> with user tracking table
            <input type="checkbox" name="pageTracking" value="true"> with page tracking(time, mouse, ...)
            <input type="submit" value="Download survey data" />
		</form>

        <hr />
		<h3>URLs</h3>
        <%=urlsTable%>
		<hr />

        <form action="Admin" method="get">
            <input type="hidden" name="mode" value="refresh" />
            <input type="submit" name="refresh" value="Refresh">
        </form>

		<form action="Admin" method="post">
			<input type="submit" name="Logout" value="Logout">
		</form>

	</div>
	<div id="footer">&reg; Universit&auml;t Innsbruck</div>
</body>
</html>