<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Cheetah file upload</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/uibk.css">
    <link rel="shortcut icon" href="favicon.ico" />
    <script src="js/vendor/modernizr-2.8.0.min.js"></script>
</head>
<body>
    <!--[if lt IE 8]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div id="header"><a href="http://uibk.ac.at" target="_blank">Uibk</a></div>

    <div id="main">
        <a href="http://uibk.ac.at" target="_blank"><img id="logo" src="img/uibk.png" /></a>
        <hr />
        <div id="result">
            <h3>${requestScope["message"]}</h3>
        </div>
    </div>

    <div id="footer">
        <p id="leftfooter"><a href="Admin">Admin</a></p>
        <p id="rightfooter">&reg; Universit&auml;t Innsbruck</p>
    </div>
</body>
</html>