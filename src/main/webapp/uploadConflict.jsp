<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Cheetah file upload</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/uibk.css">
    <link rel="shortcut icon" href="favicon.ico" />
    <script src="js/vendor/modernizr-2.8.0.min.js"></script>
</head>
<body>
    <!--[if lt IE 8]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div id="header"><a href="http://uibk.ac.at" target="_blank">Uibk</a></div>

    <div id="main">
        <a href="http://uibk.ac.at" target="_blank"><img id="logo" src="img/uibk.png" /></a>
        <hr />

        <div id="result">
            <h3 style="color: red;">Conflict</h3>
            Survey already exists, do you want override?
            <table>
                <tr>
                    <td>
                        <form action="UploadConflictServlet" method="get">
                            <input type="hidden" id="type" name="type" value="${requestScope["type"]}"/>
                            <input type="hidden" id="path" name="path" value="${requestScope["path"]}"/>
                            <input type="hidden" id="p_from" name="p_from" value="${requestScope["p_from"]}"/>
                            <input type="hidden" id="p_to" name="p_to" value="${requestScope["p_to"]}"/>
                            <input type="submit" size="10" value="Yes"/>
                        </form>
                    </td>
                    <td>
                        <form action="Admin.jsp" method="post">
                            <input type="submit" size="10" value="No"/>
                        </form>
                    </td>
                </tr>
            </table>

        </div>
    </div>

    <div id="footer">
        <p id="leftfooter"><a href="Admin">Admin</a></p>
        <p id="rightfooter">&reg; Universit&auml;t Innsbruck</p>
    </div>
</body>
</html>