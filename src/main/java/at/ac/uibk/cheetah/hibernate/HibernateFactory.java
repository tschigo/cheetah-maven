package at.ac.uibk.cheetah.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 *
 * @author Patrick Ober and Michael Tscholl
 * @version 0.1
 *
 */
public class HibernateFactory {

	private static ServiceRegistry serviceRegistry = null;
	private static SessionFactory sessionFactory = null;

	public HibernateFactory() {
	}

	/**
	 * Create a sessionFactory and returns it
	 *
	 * @return a sessionFactory Object
	 */
	public static SessionFactory create() {
		Configuration configuration = new Configuration();
		configuration.configure();

		serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
				configuration.getProperties()).build();

		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		return sessionFactory;
	}

	/**
	 * Close a sessionFactory
	 */
	public static void close() {
		sessionFactory.close();
	}

	/**
	 * A helper methods which exec a select and returns it's result as as List
	 * of Objects
	 * 
	 * @param session
	 * @param query
	 * @return
	 */
	public static List<?> select(Session session, String query) {
		Query q = session.createQuery(query);
		return q.list();
	}

	public static Object selectFirst(Session session, String query) {
		Query q = session.createQuery(query);
		if (q.list().size() == 0) {
			return null;
		}
		return q.list().get(0);
	}

}