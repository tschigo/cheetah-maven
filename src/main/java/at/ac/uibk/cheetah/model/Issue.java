package at.ac.uibk.cheetah.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author Patrick Ober and Michael Tscholl
 * @version 0.1
 *
 */
@Entity
public class Issue {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@OneToOne
	@JoinColumn(name = "inputField_id")
	private InputField inputField;

	@OneToOne
	@JoinColumn(name = "image_id")
	private Image image;

	@ManyToOne
	@JoinColumn(name = "page_id")
	private Page page;

	@OneToMany(mappedBy = "issue")
	private List<Answer> answers;

	private String question;
	private String hint;

	@Column(columnDefinition = "BIT", length = 1)
	private Boolean required;
	private String name;
	private String style;
	private String dependency;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public InputField getInputField() {
		return inputField;
	}

	public void setInputField(InputField inputField) {
		this.inputField = inputField;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getDependency() {
		return dependency;
	}

	public void setDependency(String dependency) {
		this.dependency = dependency;
	}

	@Override
	public String toString() {
		return "Issue [id=" + id + ", inputField=" + inputField + ", image="
				+ image + ", page=" + page + ", answers=" + answers
				+ ", question=" + question + ", hint=" + hint + ", required="
				+ required + ", name=" + name + ", style=" + style
				+ ", dependency=" + dependency + "]";
	}

}
