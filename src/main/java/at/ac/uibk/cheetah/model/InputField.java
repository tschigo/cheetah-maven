package at.ac.uibk.cheetah.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Patrick Ober and Michael Tscholl
 * @version 0.1
 *
 */
@Entity
public class InputField {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private int size;
	private String type;
	private int minlength;
	private int maxlength;
	private int min;
	private int max;
	private String value;

	@OneToMany(mappedBy = "inputField")
	private List<OptionField> optionFields = new ArrayList<OptionField>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getMinlength() {
		return minlength;
	}

	public void setMinlength(int minlength) {
		this.minlength = minlength;
	}

	public int getMaxlength() {
		return maxlength;
	}

	public void setMaxlength(int maxlength) {
		this.maxlength = maxlength;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public List<OptionField> getOptionFields() {
		return optionFields;
	}

	public void setOptionFields(List<OptionField> optionFields) {
		this.optionFields = optionFields;
	}

}
