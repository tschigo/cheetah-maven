package at.ac.uibk.cheetah.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Patrick Ober and Michael Tscholl
 * @version 0.1
 *
 */
@Entity
public class OptionField {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String value;
	private String text;

	@ManyToOne
	@JoinColumn(name = "inputField_id")
	private InputField inputField;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public InputField getInputField() {
		return inputField;
	}

	public void setInputField(InputField inputField) {
		this.inputField = inputField;
	}

}
