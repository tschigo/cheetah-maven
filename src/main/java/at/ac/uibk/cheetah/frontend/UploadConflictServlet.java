package at.ac.uibk.cheetah.frontend;

import at.ac.uibk.cheetah.mapper.Xml2DB;
import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * Created on 16/01/15.
 */
@WebServlet("/UploadConflictServlet")
public class UploadConflictServlet extends HttpServlet {


    /**
	 * 
	 */
	private static final long serialVersionUID = 6443051585597996724L;

	/**
     * @see javax.servlet.http.HttpServlet#HttpServlet()
     */
    public UploadConflictServlet() {
        super();
    }

    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        String type = request.getParameter("type");
        String path = request.getParameter("path");


        for(Map.Entry<String, String[]> m : request.getParameterMap().entrySet()){
            System.out.println("" + m.getKey() + "\t" + m.getValue()[0]);
        }
        Xml2DB xml2db = new Xml2DB();
        switch (type){
            case "xml":
                xml2db.insertXML(path);
                break;
            case "zip":
                FileUtils.copyDirectory(new File(request.getParameter("p_from")), new File(request.getParameter("p_to")));

                xml2db.insertXML(path);
                break;
            default:
                break;
        }


        request.setAttribute("message", "File uploaded successfully<br /><a href=\"Page?mode=refresh\"> refresh </a>");
        request.getRequestDispatcher("upload.jsp").forward(request, response);


    }
}