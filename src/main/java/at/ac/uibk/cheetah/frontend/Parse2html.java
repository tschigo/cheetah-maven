package at.ac.uibk.cheetah.frontend;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import org.hibernate.Query;
import org.hibernate.Session;

import at.ac.uibk.cheetah.hibernate.HibernateFactory;
import at.ac.uibk.cheetah.model.Image;
import at.ac.uibk.cheetah.model.InputField;
import at.ac.uibk.cheetah.model.Issue;
import at.ac.uibk.cheetah.model.OptionField;
import at.ac.uibk.cheetah.model.Page;
import at.ac.uibk.cheetah.model.Survey;

/**
 *
 * @author Patrick Ober and Michael Tscholl
 * @version 0.1
 *
 */
public class Parse2html {

	Session session;
	String path;

	public Map<String, List<String>> createHtml(String path) {
		this.path = path;
		Map<String, List<String>> html = new TreeMap<>();

		this.session = HibernateFactory.create().openSession();
		this.session.beginTransaction();

		List<?> list = HibernateFactory.select(session,
				"from Survey WHERE active=1");

		for (Object s : list) {
			html.put(((Survey) s).getTitle(), createHtmlSurvey(((Survey) s)));
		}

		session.close();
		HibernateFactory.close();
		return html;
	}

	private List<String> createHtmlSurvey(Survey survey) {
		List<String> html = new ArrayList<>();
		Query query = this.session
				.createQuery("from Page where survey.title = '"
						+ survey.getTitle() + "'");

		int pageNumber = 1;
		for (Object p : query.list()) {
			html.add(createHtmlPage((Page) p, pageNumber++));
		}

		return html;
	}

	private String createHtmlPage(Page page, int pageNumber) {
		StringBuilder html = new StringBuilder();
		Scanner scanner = null;
		String template = page.getSurvey().getConfig().getTemplate();

		if (template == null) {
			template = "default.html";
		}

		try {
			File file = new File(this.path + template);
			scanner = new Scanner(file);
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				if (line.toLowerCase().contains("id=\"cheetah\"")
						|| line.toLowerCase().contains("id='cheetah'")) {
					break;
				} else if (line.toLowerCase().contains("</head>")) {
					html.append("<script src=\"js/vendor/modernizr-2.8.0.min.js\"></script>\n");
					html.append("<script src=\"js/vendor/jquery-1.11.1.min.js\"></script>\n");
					html.append("<script src=\"js/other/jquery.validate.min.js\"></script>\n");
					html.append("<script src=\"js/other/additional-methods.min.js\"></script>\n");
					html.append("<script type=\"text/javascript\" src=\"js/myscript.js\"></script>\n");

					html.append("<script>\n");
					html.append("(function() {");
					html.append("var p = [], w = window, d = document, e = f = 0;");
					html.append("p.push('ua=' + encodeURIComponent(navigator.userAgent));");
					html.append("e |= w.ActiveXObject ? 1 : 0;");
					html.append("e |= w.opera ? 2 : 0;");
					html.append("e |= w.chrome ? 4 : 0;");
					html.append("e |= 'getBoxObjectFor' in d || 'mozInnerScreenX' in w ? 8 : 0;");
					html.append("e |= ('WebKitCSSMatrix' in w || 'WebKitPoint' in w");
					html.append("|| 'webkitStorageInfo' in w || 'webkitURL' in w) ? 16 : 0;");
					html.append("e |= (e & 16 && ({}.toString).toString().indexOf(\"\\n\") === -1) ? 32 : 0;");
					html.append("p.push('e=' + e);");
					html.append("f |= 'sandbox' in d.createElement('iframe') ? 1 : 0;");
					html.append("f |= 'WebSocket' in w ? 2 : 0;");
					html.append("f |= w.Worker ? 4 : 0;");
					html.append("f |= w.applicationCache ? 8 : 0;");
					html.append("f |= w.history && history.pushState ? 16 : 0;");
					html.append("f |= d.documentElement.webkitRequestFullScreen ? 32 : 0;");
					html.append("f |= 'FileReader' in w ? 64 : 0;");
					html.append("p.push('f=' + f);");
					html.append("p.push('r=' + Math.random().toString(36).substring(7));");
					html.append("p.push('w=' + screen.width);");
					html.append("p.push('h=' + screen.height);");
					html.append("var s = d.createElement('script');");
					html.append("s.src = 'http://api.whichbrowser.net/dev/detect.js?' + p.join('&');");
					html.append("d.getElementsByTagName('head')[0].appendChild(s);");
					html.append("})();\n");
					html.append("function preventBack() {window.history.forward();} setTimeout(\"preventBack()\", 0); window.onunload = function () {null};\n");
					html.append("</script>\n");
				} else if (line.toLowerCase().contains("</head>")) {
					line = "<body>";
				}
				html.append(line + "\n");
			}
			html.append("<div id=\"cheetah\">\n");
			html.append("<form name=\"formular\" id=\"formular\" method=\"post\" action=\"Page\">\n");

			html.append("<div id=\"page\">Page " + pageNumber + "</div>\n");
			html.append("\t<input type=\"hidden\" id=\"mouse\" />\n");
			html.append("\t<input type=\"hidden\" id=\"scroll\" />\n");
			html.append("\t<input type=\"hidden\" id=\"time\" />\n");

			Boolean focus = true;
			for (Issue i : page.getIssues()) {
				html.append(createIssueHtml(i, focus));
				focus = false;
			}

			html.append("<input type=\"hidden\" value=\"" + pageNumber
					+ "\" name=\"page\" />\n");
			html.append("<input type=\"hidden\" value=\"" + page.getId()
					+ "\" name=\"pageId\" />\n");
			html.append("<input type=\"hidden\" value=\""
					+ page.getSurvey().getTitle() + "\" name=\"survey\" />\n");

			html.append("<input type=\"submit\" value=\"Next\" />\n");
			html.append("</form>\n");
			html.append("</div>\n");

			html.append(createJS(page));

			while (scanner.hasNext()) {
				html.append(scanner.nextLine() + "\n");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			scanner.close();
		}

		return html.toString();
	}

	private String createIssueHtml(Issue issue, Boolean focus) {
		StringBuilder html = new StringBuilder();
		html.append("<section>\n");
		html.append("<div id=\"question\">" + issue.getQuestion() + "</div>\n");

		if (issue.getHint() != null) {
			html.append("  <div id=\"hint\">" + issue.getHint() + "</div>\n");
		}
		if (issue.getImage() != null) {
			html.append(createImageHtml(issue.getImage()));
		}

		html.append(createInputFieldHtml(issue.getInputField(), focus));
		html.append("</section>\n");
		return html.toString();
	}

	private static String createImageHtml(Image image) {
		StringBuilder html = new StringBuilder();
		html.append("<img src=\"" + image.getImage() + "\"");
		if (image.getWidth() != 0) {
			html.append(" width=\"" + image.getWidth() + "\"");
		}
		if (image.getHeight() != 0) {
			html.append(" height=\"" + image.getHeight() + "\"");
		}
		html.append(" />\n");
		return html.toString();
	}

	private String createInputFieldHtml(InputField in, Boolean focus) {
		StringBuilder html = new StringBuilder();

		if (in.getType().equals("radio") || in.getType().equals("checkbox")) {

			html.append("<p id=\"topics_q" + in.getId() + "\" ");
			if (focus) {
				html.append("autofocus");
			}
			html.append(">\n");

			for (OptionField o : in.getOptionFields()) {
				html.append("\t<label for=\"q" + o.getInputField().getId()
						+ "\">\n");
				html.append("\t\t<input type=\"" + in.getType() + "\"");
				html.append(" name=\"q" + o.getInputField().getId() + "\"");
				html.append(" id=\"id" + o.getInputField().getId() + "_"
						+ o.getId() + "\"");

				html.append(" size=\"" + in.getSize() + "\"");
				html.append(" value=\"" + o.getText() + "\"");
				html.append(" >");
				html.append(o.getText());
				html.append("</input>\n");

				html.append("\t</label>\n");

			}
			html.append("</p>\n");
		} else if (in.getType().equals("select")) {
			html.append("<select size=\"1\" name=\"q" + in.getId() + "\" ");
			if (focus) {
				html.append("autofocus");
			}
			html.append(">\n");

			for (OptionField o : in.getOptionFields()) {
				html.append("<option name=\"q" + in.getId() + "\"");
				html.append(" id=\"id" + o.getInputField().getId() + "_"
						+ o.getId() + "\"");
				html.append(" value=\"" + o.getText() + "\"");
				html.append(" >");
				html.append(o.getText());
				html.append("</option>\n");
			}
			html.append("</select>");
		} else if (in.getType().equals("textbox")) {
			html.append("<textarea rows=\"8\" cols=\"70\" maxlength=\"1000\" name=\"q"
					+ in.getId() + "\" ");
			if (focus) {
				html.append("autofocus");
			}
			html.append(">\n");

			html.append("</textarea>\n");
		} else {
			html.append("<input type=\"" + in.getType() + "\"");
			html.append(" name=\"q" + in.getId() + "\"");
			html.append(" id=\"id" + in.getId() + "\"");
			html.append(" size=\"" + in.getSize() + "\"");
			if (in.getValue() != null) {
				html.append(" value=\"" + in.getValue() + "\"");
			}
			if (in.getMinlength() != 0)
				html.append(" minlength=\"" + in.getMinlength() + "\"");
			if (in.getMaxlength() != 0)
				html.append(" maxlength=\"" + in.getMaxlength() + "\"");
			else {
				html.append(" maxlength=\"100\"");
			}

			if (focus) {
				html.append(" autofocus");
			}
			html.append(" />\n");
		}

		return html.toString();
	}

	private String resolveDependency(Page page, Issue issue) {
		String id = "id";
		for (Issue i : page.getIssues()) {
			if (issue.getDependency().split(":")[0].equals(i.getName())) {
				id += i.getInputField().getId()
						+ "_"
						+ i.getInputField()
								.getOptionFields()
								.get(Integer.parseInt(issue.getDependency()
										.split(":")[1]) - 1).getId();
			}
		}
		return id;
	}

	private String resolveQ(Page page, Issue issue) {
		for (Issue i : page.getIssues()) {
			if (issue.getDependency().split(":")[0].equals(i.getName())) {
				return "q" + i.getId();
			}
		}
		return "ERROR";
	}

	private String resolveQ_type(Page page, Issue issue) {
		for (Issue i : page.getIssues()) {
			if (issue.getDependency().split(":")[0].equals(i.getName())) {
				return i.getInputField().getType();
			}
		}
		return "ERROR";
	}

	private String createDependenciesJS(Page p) {
		StringBuilder js = new StringBuilder();

		js.append("\t$().ready(function() {\n");
		js.append("\t\t$(\"#formular\").validate({\n");
		js.append("\t\t\trules: {\n");
		for (Issue issue : p.getIssues()) {
			if (issue.getRequired() || issue.getInputField().getMax() != 0
					|| issue.getInputField().getMin() != 0
					|| !issue.getInputField().getType().equals("text")) {

				js.append("\t\t\t\tq" + issue.getInputField().getId() + ": {\n");

				if (issue.getRequired()) {
					if (issue.getDependency() == null) {
						js.append("\t\t\t\t\trequired: true,\n");
					} else {
						js.append("\t\t\t\t\trequired: \"#"
								+ resolveDependency(p, issue) + ":checked\"");
					}
				}

				if (issue.getInputField().getType().equals("date")) {
					js.append("\t\t\t\t\tdate: true,\n");
				}

				if (issue.getInputField().getType().equals("email")) {
					js.append("\t\t\t\t\temail: true,\n");
				}

				if (issue.getInputField().getType().equals("number")) {
					js.append("\t\t\t\t\tnumber: true,\n");

					// range
					if (issue.getInputField().getMax() != 0) {
						js.append("\t\t\t\t\tmax: "
								+ issue.getInputField().getMax() + ",\n");
					}

					if (issue.getInputField().getMin() != 0) {
						js.append("\t\t\t\t\tmin: "
								+ issue.getInputField().getMin() + ",\n");
					}

				}

				// del last ,
				js.replace(js.length() - 2, js.length() - 1, "");

				js.append("\t\t\t\t},\n");
			}

		}
		// del last ,
		js.replace(js.length() - 2, js.length() - 1, "");

		js.append("\t\t\t}\n");
		js.append("\t\t});\n");

		// check for dependencies
		for (Issue issue : p.getIssues()) {
			if (issue.getDependency() != null
					&& resolveQ_type(p, issue).equals("select")) {
				String id = resolveDependency(p, issue);
				js.append("\n\n");
				js.append("\t\tvar check_" + issue.getId() + " = $(\"#" + id
						+ "\").is(\":selected\");\n");
				js.append("\t\tvar color_" + issue.getId() + " = $(\"#topics_q"
						+ issue.getId() + "\")[check_" + issue.getId()
						+ " ? \"removeClass\" : \"addClass\"](\"gray\");\n");
				js.append("\t\tvar input_" + issue.getId() + " = color_"
						+ issue.getId()
						+ ".find(\"input\").attr(\"disabled\", !check_"
						+ issue.getId() + ")\n\n");

				js.append("$('select[name=\"" + resolveQ(p, issue)
						+ "\"]').on('change', function() {\n");
				js.append("\tif(document.getElementById('" + id
						+ "').selected){\n");
				if (issue.getInputField().getType().equals("text")) {
					js.append("\t\tdocument.getElementById(\"id"
							+ issue.getInputField().getId()
							+ "\").disabled = false;");
				} else {

					js.append("\t\tcolor_" + issue.getId()
							+ ".removeClass(\"gray\");\n");
					js.append("\t\tinput_" + issue.getId()
							+ ".attr(\"disabled\", false);\n");
				}
				js.append("\t}else if (!document.getElementById('" + id
						+ "').selected) {\n");
				if (issue.getInputField().getType().equals("text")) {
					js.append("\t\tdocument.getElementById(\"id"
							+ issue.getInputField().getId()
							+ "\").disabled = true;");
				} else {
					js.append("\t\tcolor_" + issue.getId()
							+ ".addClass(\"gray\");\n");
					js.append("\t\tinput_" + issue.getId()
							+ ".attr(\"disabled\", true);\n");
				}
				js.append("\t}\n");
				js.append("});\n");

				js.append("\n\n");
			} else if (issue.getDependency() != null
					&& (resolveQ_type(p, issue).equals("radio") || resolveQ_type(
							p, issue).equals("checkbox"))) {
				String id = resolveDependency(p, issue);
				js.append("\n\n");
				js.append("\t\tvar check_" + issue.getId() + " = $(\"#" + id
						+ "\").is(\":checked\");\n");

				js.append("\t\tvar color_" + issue.getId() + " = $(\"#topics_q"
						+ issue.getId() + "\")[check_" + issue.getId()
						+ " ? \"removeClass\" : \"addClass\"](\"gray\");\n");
				js.append("\t\tvar input_" + issue.getId() + " = color_"
						+ issue.getId()
						+ ".find(\"input\").attr(\"disabled\", !check_"
						+ issue.getId() + ")\n\n");
				js.append("$('input[name=\"" + resolveQ(p, issue)
						+ "\"]').on('change', function() {\n");
				js.append("\tif(document.getElementById('" + id
						+ "').checked){\n");
				if (issue.getInputField().getType().equals("text")) {
					js.append("\t\tdocument.getElementById(\"id"
							+ issue.getInputField().getId()
							+ "\").disabled = false;");
				} else {
					js.append("\t\tcolor_" + issue.getId()
							+ ".removeClass(\"gray\");\n");
					js.append("\t\tinput_" + issue.getId()
							+ ".attr(\"disabled\", false);\n");
				}

				js.append("\t}else if (!document.getElementById('" + id
						+ "').checked) {\n");

				if (issue.getInputField().getType().equals("text")) {
					js.append("\t\tdocument.getElementById(\"id"
							+ issue.getInputField().getId()
							+ "\").disabled = true;");
				} else {

					js.append("\t\tcolor_" + issue.getId()
							+ ".addClass(\"gray\");\n");
					js.append("\t\tinput_" + issue.getId()
							+ ".attr(\"disabled\", true);\n");

				}
				js.append("\t}\n");
				js.append("});\n");

				js.append("\n\n");
			}
		}
		js.append("\t});\n");

		return js.toString();
	}

	private String createJS(Page p) {
		StringBuilder js = new StringBuilder();
		js.append("<script>\n");

		js.append(createDependenciesJS(p));

		js.append("\tdocument.getElementById(\"time\").value = new Date().getTime();\n");
		js.append("\n");

		js.append("\twaitForWhichBrowser(function() {\n");
		js.append("\t\ttry {\n");
		js.append("\t\t\tbrowser = new WhichBrowser({\n");
		js.append("\t\t\t\tuseFeatures : true,\n");
		js.append("\t\t\t\tdetectCamouflage : true\n");
		js.append("\t\t\t});\n");
		js.append("\t\t\t$('#formular').append('<input type=\"hidden\" name=\"browser\" id=\"browser\" value=\"' + browser + '\" />');\n");
		js.append("\t\t} catch (e) {\n");
		js.append("\t\t\to.innerHTML = 'Oops, something went wrong:<br> <small>' + e	+ '</small>';\n");
		js.append("\t\t}\n");
		js.append("\t});\n");
		js.append("\n");

		js.append("\tgetMousePosition();\n");
		js.append("\tgetScrollPosition();\n");
		js.append("\n");

		js.append("\t$(\"#formular\").submit(\n");
		js.append("\t\tfunction(event) {\n");

		js.append("if(getParameterByName(\"mode\") == \"test\"){$('#formular').append('<input type=\"hidden\" name=\"mode\" id=\"mode\" value=\"test\" />');}\n");

		js.append("\t\t\t$('#formular').append('<input type=\"hidden\" name=\"screen\" id=\"screen\" value=\"' + window.screen.width + '/' + window.screen.height + '\" />');\n");
		js.append("\t\t\t$('#formular').append('<input type=\"hidden\" name=\"mouse\" id=\"mouse\" value=\"' + $('#mouse').val() + '\" />');\n");
		js.append("\t\t\t$('#formular').append('<input type=\"hidden\" name=\"scroll\" id=\"scroll\" value=\"' + $('#scroll').val() + '\" />');\n");
		js.append("\t\t\t$('#formular').append('<input type=\"hidden\" name=\"time\" id=\"time\" value=\"' + (new Date().getTime() - $('#time').val())	+ '\" />');\n");
		js.append("\t\t}\n");
		js.append("\t);\n\n");
		js.append("</script>\n");

		return js.toString();
	}
}
