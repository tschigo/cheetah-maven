package at.ac.uibk.cheetah.frontend;

import at.ac.uibk.cheetah.hibernate.HibernateFactory;
import at.ac.uibk.cheetah.model.Survey;
import org.hibernate.Session;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Patrick Ober and Michael Tscholl
 * @version 0.1
 *
 */
public class PageInfo {

	/**
	 * 
	 * @return a map of where the key is the survey name and a Pair where the
	 *         first Object the release Date is and the second Object the
	 *         expiring data is
	 */
	public Map<String, Pair<Date, Date>> getDates() {

		Map<String, Pair<Date, Date>> map = new TreeMap<>();
		Session session = HibernateFactory.create().openSession();
		session.beginTransaction();

		List<?> list = HibernateFactory.select(session, "FROM Survey");

		session.close();
		HibernateFactory.close();

		for (Object s : list) {
			Pair<Date, Date> p = new Pair<Date, Date>(((Survey) s).getConfig()
					.getReleasedate(), ((Survey) s).getConfig()
					.getExpiringdate());
			map.put(((Survey) s).getTitle(), p);
		}

		return map;
	}

}
