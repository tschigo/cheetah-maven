package at.ac.uibk.cheetah.frontend;

import java.io.*;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import at.ac.uibk.cheetah.mapper.Xml2DB;

import org.apache.commons.io.FileUtils;

/**
 * @author Patrick Ober and Michael Tscholl
 * @version 0.1
 */
@WebServlet("/UploadServlet")
public class UploadServlet extends HttpServlet {
    private static final long serialVersionUID = 1676076635444690385L;
    private static String UPLOAD_DIR = "/tmp/";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        // process only if its multipart content
        if (ServletFileUpload.isMultipartContent(request)) {
            try {
                String res = null;
                List<?> multiparts = new ServletFileUpload(
                        new DiskFileItemFactory()).parseRequest(request);

                for (Object o : multiparts) {
                    FileItem item = (FileItem) o;

                    // upload to UPLOAD_DIR
                    if (!item.isFormField()) {
                        item.write(new File(UPLOAD_DIR + item.getName()));
                    }

                    Xml2DB xml2db = new Xml2DB();
                    String schema = getServletContext().getRealPath("/cheetah.xsd");
                    switch (item.getContentType()){
                        case "text/xml":
                            // check if it is valid
                            res = xml2db.validateXMLSchema(schema, UPLOAD_DIR + item.getName());

                            // it is valid
                            if(res == null){
                                if(xml2db.surveyExists(UPLOAD_DIR + item.getName())){
                                    // ask what to do;
                                    request.setAttribute("type", "xml");
                                    request.setAttribute("path", UPLOAD_DIR + item.getName());
                                    request.getRequestDispatcher("uploadConflict.jsp").forward(request, response);
                                    return;
                                }else{
                                    // File uploaded successfully and xml is valid
                                    xml2db.insertXML(UPLOAD_DIR + item.getName());
                                    res = "File uploaded successfully<br /><a href=\"Page?mode=refresh\"> refresh </a>";
                                }
                            }else{
                                res = "xml is not valid <br />" + res;
                            }
                            break;
                        case "application/zip":
                        case "application/x-zip-compressed":

                            // unzip
                            extract(UPLOAD_DIR + item.getName());

                            // check if it is valid
                            String sub = item.getName().substring(0, item.getName().length() - 4) + "/";
                            String xml = UPLOAD_DIR + sub + sub + "survey.xml";
                            File up = new File(getServletContext().getRealPath("") + File.separator + "cheetah_upload");
                            res = xml2db.validateXMLSchema(schema, xml);
                            if(res == null){
                                if(xml2db.surveyExists(xml)){
                                    // ask what to do;
                                    request.setAttribute("type", "zip");
                                    request.setAttribute("path", xml);
                                    request.setAttribute("p_from", UPLOAD_DIR + sub);
                                    request.setAttribute("p_to", up);
                                    request.getRequestDispatcher("uploadConflict.jsp").forward(request, response);
                                    return;
                                }else{
                                    // File uploaded successfully and xml is valid
                                    FileUtils.copyDirectory(new File(UPLOAD_DIR + sub), up);
                                    xml2db.insertXML(xml);
                                    res = "File uploaded successfully<br /><a href=\"Page?mode=refresh\"> refresh </a>";
                                }
                            }else{
                                res = "xml is not valid <br />" + res;
                            }
                            break;
                        default:
                            res = "Sorry wrong content type!";

                    }
                }
                if(res != null)
                    request.setAttribute("message", res);
                else
                    request.setAttribute("message", "fuck it");
            } catch (Exception e) {
                e.printStackTrace();
                request.setAttribute("message", "File Upload Failed due to "
                        + e);
            }

        }
        request.getRequestDispatcher("upload.jsp").forward(request, response);
    }

    @SuppressWarnings({ "rawtypes", "resource" })
	public void extract(String zipFile) throws ZipException, IOException {
        int BUFFER = 2048;
        File file = new File(zipFile);

        ZipFile zip = new ZipFile(file);
        String newPath = zipFile.substring(0, zipFile.length() - 4);

        new File(newPath).mkdir();
        Enumeration zipFileEntries = zip.entries();

        // Process each entry
        while (zipFileEntries.hasMoreElements()) {
            // grab a zip file entry
            ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
            String currentEntry = entry.getName();
            File destFile = new File(newPath, currentEntry);
            //destFile = new File(newPath, destFile.getName());
            File destinationParent = destFile.getParentFile();

            // create the parent directory structure if needed
            destinationParent.mkdirs();

            if (!entry.isDirectory()) {
                BufferedInputStream is = new BufferedInputStream(zip
                        .getInputStream(entry));
                int currentByte;
                // establish buffer for writing file
                byte data[] = new byte[BUFFER];

                // write the current file to disk
                FileOutputStream fos = new FileOutputStream(destFile);
                BufferedOutputStream dest = new BufferedOutputStream(fos,
                        BUFFER);

                // read and write until last byte is encountered
                while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
                    dest.write(data, 0, currentByte);
                }
                dest.flush();
                dest.close();
                is.close();
            }

            if (currentEntry.endsWith(".zip")) {
                // found a zip file, try to open
                extract(destFile.getAbsolutePath());
            }
        }
    }
}
