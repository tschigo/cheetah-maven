package at.ac.uibk.cheetah.frontend;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import at.ac.uibk.cheetah.backend.InputReader;
import at.ac.uibk.cheetah.hibernate.HibernateFactory;
import at.ac.uibk.cheetah.model.Survey;

import org.hibernate.Session;

/**
 *
 * @author Patrick Ober and Michael Tscholl
 * @version 0.1
 *
 */
@WebServlet("/Page")
public class PageServlet extends HttpServlet {

	private static final long serialVersionUID = -4921845255104680127L;
	private Map<String, List<String>> html = new TreeMap<>();
	private Map<String, Pair<Date, Date>> dates = new TreeMap<>();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PageServlet() {
		super();
	}

	@Override
	public void init() throws ServletException {
		super.init();
		Parse2html parse2html = new Parse2html();
		this.html = parse2html.createHtml(getServletContext().getRealPath("/"));
		PageInfo pageInfo = new PageInfo();
		this.dates = pageInfo.getDates();

	}

	@Override
	public void destroy() {
		super.destroy();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if (request.getParameter("mode") != null) {
			switch (request.getParameter("mode")) {
			case "refresh":
				Parse2html parse2html = new Parse2html();
				this.html = parse2html.createHtml(getServletContext()
						.getRealPath("/"));
				PageInfo pageInfo = new PageInfo();
				this.dates = pageInfo.getDates();
				break;
			case "test":
				String survey = request.getParameter("survey");
				int page;
				try {
					page = Integer.parseInt(request.getParameter("page"));
				} catch (NumberFormatException e) {
					page = 0;
				}

				if (survey == null) {
					RequestDispatcher rd = getServletContext()
							.getRequestDispatcher("/");
					PrintWriter out = response.getWriter();
					out.println("<font color=red>No survey was selected</font>");
					rd.include(request, response);
				} else if (this.html.get(survey) == null) {
					RequestDispatcher rd = getServletContext()
							.getRequestDispatcher("/");
					PrintWriter out = response.getWriter();
					out.println("<font color=red>No survey was found</font>");
					rd.include(request, response);
				}

				if (page >= this.html.get(survey).size()) {
					response.sendRedirect("Admin");
					return;
				}

				response.getWriter().write(this.html.get(survey).get(page));
				return;
			}
			response.sendRedirect("index.jsp");
			return;
		}

		// we are in normal mode
		Pair<String, Integer> page = getNextPage(request);

		if (page == null) {
			response.sendRedirect("index.jsp");
			return;
		}

		// check if survey exist -> 404 survey not found
		if (this.html.get(page.getFirst()) == null) {
			response.sendRedirect("index.jsp");
			return;
		}

		// survey finished
		if ((this.html.get(page.getFirst()).size() - 1) < page.getSecond()) {
			createPassiveCookie(request, response, page,
					getCooldownTime(page.getFirst()) * 60);
			destroyCookie(request, response);
			response.sendRedirect("finish.html");
			return;
		}

		// Check for expire/release date
		Date date = new Date();
		try {
			if (date.before(this.dates.get(page.getFirst()).getFirst())) {
				response.sendRedirect("releaseDate.jsp");
				return;
			}
		} catch (NullPointerException e) {

		}
		try {
			if (date.after(this.dates.get(page.getFirst()).getSecond())) {
				response.sendRedirect("expiringDate.jsp");
				return;
			}
		} catch (NullPointerException e) {

		}

		// write cookie and deliver survey
		createActiveCookie(request, response, page, 60 * 60);
		response.getWriter().write(
				this.html.get(page.getFirst()).get(page.getSecond()));

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		//TODO bad alaba
	}

	private int getCooldownTime(String survey) {
		Session session = HibernateFactory.create().openSession();
		session.beginTransaction();
		List<?> surveys = session.createQuery(
				"FROM Survey WHERE title = '" + survey + "'").list();
		session.close();
		HibernateFactory.close();

		return ((Survey) surveys.get(0)).getConfig().getCooldown();
	}

	private void createActiveCookie(HttpServletRequest request,
									HttpServletResponse response, Pair<String, Integer> page,
									int cooldown) {
		// Survey and page number
		Cookie cookieSurvey = new Cookie("active", page.getFirst());
		cookieSurvey.setMaxAge(cooldown);
		response.addCookie(cookieSurvey);

		Cookie cookiePageNr = new Cookie("pageNr", String.valueOf(page
				.getSecond()));
		cookiePageNr.setMaxAge(cooldown);
		response.addCookie(cookiePageNr);


		// test if old cookie exist
		String session = request.getSession().getId();
		for (Cookie cookie : request.getCookies()) {
			if (cookie.getName().equals("session")) {
				session = cookie.getValue();
			}
		}

		Cookie cookieSession = new Cookie("session", session);
		cookieSession.setMaxAge(cooldown);
		response.addCookie(cookieSession);


	}

	private void createPassiveCookie(HttpServletRequest request,
									 HttpServletResponse response, Pair<String, Integer> page,
									 int cooldown) {
		Cookie cookiePageNr = new Cookie(page.getFirst(), "Alaba Fussballgott!");
		cookiePageNr.setMaxAge(cooldown);
		response.addCookie(cookiePageNr);
	}

	private void destroyCookie(HttpServletRequest request,
							   HttpServletResponse response) {
		Cookie cookieSurvey = new Cookie("active", "null");
		cookieSurvey.setMaxAge(0);
		response.addCookie(cookieSurvey);
		Cookie cookiePageNr = new Cookie("pageNr", "-1");
		cookiePageNr.setMaxAge(0);
		response.addCookie(cookiePageNr);
	}

	private Pair<String, Integer> getNextPage(HttpServletRequest request) {
		int pageNr = -1;
		String survey = null;

		// check if there is a cookie
		if (request.getCookies() != null) {
			for (Cookie cookie : request.getCookies()) {
				switch (cookie.getName()) {
					case "active":
						survey = cookie.getValue();
						break;
					case "pageNr":
						Integer pageID = null;
						pageNr = Integer.parseInt(cookie.getValue());
						try {
							pageID = Integer.parseInt(request.getParameter("page"));
						} catch (NumberFormatException e) {
							// pageNr is correct
							;
						}
						if (pageID == null) {
							// pageNr is correct;
						} else if (pageID == pageNr) {
							// pageNr is correct
							;
						} else {
							pageNr = Integer.parseInt(cookie.getValue()) + 1;
						}
						break;
				}
			}


			if (survey != null && pageNr != -1) {
				System.out.println(survey + " \t" + pageNr);

				//TODO remove debug info
				debug(request);

				if(request.getParameterMap().size() > 3){

					String session = request.getSession().getId();
					for (Cookie cookie : request.getCookies()) {
						if (cookie.getName().equals("session")) {
							session = cookie.getValue();
						}
					}

					System.out.println("Session\t" + session);

					InputReader inputReader = new InputReader();
					inputReader.write2DB(request.getParameterMap(), survey, session);
				}
				return new Pair<String, Integer>(survey, pageNr);
			}

		}

		// no cookie, GET/POST as parameter
		survey = request.getParameter("survey");
		if (survey != null) {

			// check for suvey cooldown
			if (request.getCookies() != null) {
				for (Cookie cookie : request.getCookies()) {
					if (cookie.getName().equals(survey)) {
						return null;
					}
				}
			}

			return new Pair<String, Integer>(survey, 0);
		}

		// else
		return null;
	}

	private void debug(HttpServletRequest request) {
		System.out
				.println("\n\n========================== GET ==================================\n");
		for (Map.Entry<String, String[]> m : request.getParameterMap()
				.entrySet()) {
			System.out.print("key: " + m.getKey() + " \t value: ");
			for (String s : m.getValue()) {
				System.out.print(s + " ");
			}
			System.out.println();
		}
		System.out.println("\t\t =========");

	}
}
