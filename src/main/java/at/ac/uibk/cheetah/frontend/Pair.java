package at.ac.uibk.cheetah.frontend;

/**
 * A simple generic class which contains 2 object
 * 
 * @author Patrick Ober and Michael Tscholl
 * @version 0.1
 *
 */
public class Pair<F, S> {
	private F first;
	private S second;

	/**
	 * 
	 * @param first
	 *            Object of type F
	 * @param second
	 *            Object of type S
	 */
	public Pair(F first, S second) {
		this.first = first;
		this.second = second;
	}

	public void setFirst(F first) {
		this.first = first;
	}

	public void setSecond(S second) {
		this.second = second;
	}

	public F getFirst() {
		return first;
	}

	public S getSecond() {
		return second;
	}

	@Override
	public String toString() {
		return "Pair[ " + first + " / " + second + " ]";
	}

}