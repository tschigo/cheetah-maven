package at.ac.uibk.cheetah.frontend;

import at.ac.uibk.cheetah.backend.ExportSurvey;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Patrick Ober and Michael Tscholl
 * @version 0.1
 *
 */
@WebServlet("/DownloadServlet")
public class DownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1464638548976573284L;
	private static final int BYTES_DOWNLOAD = 1024;
	private static final String DIR = "/tmp/";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DownloadServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {

		// check if we got a survey
		String survey = request.getParameter("survey");
		if (survey == null) {
			try {
				RequestDispatcher rd = getServletContext()
						.getRequestDispatcher("Admin.jsp");
				PrintWriter out = response.getWriter();
				out.println("<font color=red>No Survey found</font>");
				rd.include(request, response);
			} catch (ServletException e) {
				e.printStackTrace();
			}
			return;
		} else {
			response.setContentType("text/plain");
			response.setHeader(
					"Content-Disposition",
					"attachment;filename="
							+ survey
							+ "_"
							+ new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss")
									.format(Calendar.getInstance().getTime())
							+ ".zip");

			ExportSurvey exportSurvey = new ExportSurvey();

			// always export Question Answer CSV
			exportSurvey.createCSVqa(survey);

			List<File> files = new ArrayList<>();
			files.add(new File(DIR + survey + "_answers.csv"));

			// both checked
			if (request.getParameter("userTracking") != null
					&& request.getParameter("pageTracking") != null) {
				exportSurvey.createCSVtracking(survey);
				exportSurvey.createCSVpageTracking(survey);

				files.add(new File(DIR + survey + "_userTracking.csv"));
				files.add(new File(DIR + survey + "_pageTracking.csv"));
			} else if (request.getParameter("userTracking") != null) {
				// User Tracking checked
				exportSurvey.createCSVtracking(survey);
				files.add(new File(DIR + survey + "_userTracking.csv"));

			} else if (request.getParameter("pageTracking") != null) {
				// Page Tracking checked
				exportSurvey.createCSVpageTracking(survey);
				files.add(new File(DIR + survey + "_pageTracking.csv"));
			}

			File zip = zip(files, survey + ".zip");

			// Download zip-file
			FileInputStream fileInputStream = new FileInputStream(zip);
			int read = 0;
			byte[] bytes = new byte[BYTES_DOWNLOAD];
			OutputStream outputStream = response.getOutputStream();

			while ((read = fileInputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			outputStream.flush();
			outputStream.close();
			fileInputStream.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	/**
	 * The method takes a list of files as in put an returns a zip file
	 *
	 * @param files
	 *            a list of files wich will be in the zip file
	 * @param filename
	 *            name of output file
	 * @return a zip file or null
	 */
	private File zip(List<File> files, String filename) {
		File zipfile = new File(filename);
		// Create a buffer for reading the files
		byte[] buf = new byte[1024];
		try {
			// create the ZIP file
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(
					zipfile));
			// compress the files
			for (int i = 0; i < files.size(); i++) {
				FileInputStream in = new FileInputStream(files.get(i));
				// add ZIP entry to output stream
				out.putNextEntry(new ZipEntry(files.get(i).getName()));
				// transfer bytes from the file to the ZIP file
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				// complete the entry
				out.closeEntry();
				in.close();
			}
			// complete the ZIP file
			out.close();
			return zipfile;
		} catch (IOException ex) {
			System.err.println(ex.getMessage());
		}
		return null;
	}
}
