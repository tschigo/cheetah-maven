package at.ac.uibk.cheetah.mapper;

import at.ac.uibk.cheetah.hibernate.HibernateFactory;
import at.ac.uibk.cheetah.model.Config;
import at.ac.uibk.cheetah.model.Image;
import at.ac.uibk.cheetah.model.InputField;
import at.ac.uibk.cheetah.model.Issue;
import at.ac.uibk.cheetah.model.OptionField;
import at.ac.uibk.cheetah.model.Page;
import at.ac.uibk.cheetah.model.Survey;

import org.hibernate.Query;
import org.hibernate.Session;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * 
 * @author Patrick Ober and Michael Tscholl
 * @version 0.2
 *
 *          This class will take as input a xml file and insert it in a database
 *
 */
public class Xml2DB {
    private String imgPath = "cheetah_upload/";

	public String validateXMLSchema(String xsdPath, String xmlPath) {
		try {
			SchemaFactory factory = SchemaFactory
					.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = factory.newSchema(new File(xsdPath));

			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new File(xmlPath)));
		} catch (IOException | SAXException e) {
			e.printStackTrace();
			return "Exception: " + e.getMessage();
		}
		return null;
	}


    public Boolean surveyExists (String pathToSurvey) {
        Boolean res = false;
        try {
            JAXBContext jc = JAXBContext
                    .newInstance(at.ac.uibk.cheetah.sitemap.Survey.class);
            Unmarshaller unmarshaller = null;
            unmarshaller = jc.createUnmarshaller();
            at.ac.uibk.cheetah.sitemap.Survey survey = (at.ac.uibk.cheetah.sitemap.Survey) unmarshaller
                    .unmarshal(new File(pathToSurvey));

            Session session = HibernateFactory.create().openSession();
            session.beginTransaction();

            // check if name exists in DB
            List<?> qExists = session
                    .createQuery("from Survey where title = '"
                            + survey.getTitle() + "'").list();


            if (qExists.size() != 0) {
                res = true;
            }

            session.getTransaction().commit();
            session.close();
            HibernateFactory.close();

        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return res;
    }


    public void insertXML(String pathToSurvey) {
		try {
			JAXBContext jc = JAXBContext
					.newInstance(at.ac.uibk.cheetah.sitemap.Survey.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();

			at.ac.uibk.cheetah.sitemap.Survey survey = (at.ac.uibk.cheetah.sitemap.Survey) unmarshaller
					.unmarshal(new File(pathToSurvey));

			Session session = HibernateFactory.create().openSession();
			session.beginTransaction();

			// check if name exists in DB

			Query qExists = session
					.createQuery("select 1 from Survey where title = '"
							+ survey.getTitle() + "'");

			if (qExists != null) {
				Query query = session.createQuery("UPDATE Survey SET active=0, title = '"
						+ survey.getTitle()
						+ "_"
						+ new SimpleDateFormat("yyyy.MM.dd_HH:mm:ss")
								.format(Calendar.getInstance().getTime())
						+ "' WHERE title = '" + survey.getTitle() + "'");
				query.executeUpdate();
			}

			Survey surveyModel = createSurvey(session, survey);

			for (at.ac.uibk.cheetah.sitemap.Survey.Page p : survey.getPage()) {
				createPage(session, p, surveyModel);
			}

			session.getTransaction().commit();
			session.close();
			HibernateFactory.close();

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	private Image createImage(Session session,
			at.ac.uibk.cheetah.sitemap.Survey.Page.Issue.Img i) {
		Image image = new Image();
		if (i.getSrc() != null)

			image.setImage(imgPath + i.getSrc());
		if (i.getHeight() != null)
			image.setHeight(i.getHeight());
		if (i.getWidth() != null)
			image.setWidth(i.getWidth());

		session.save(image);

		return image;
	}

	private InputField createInputfield(Session session,
			at.ac.uibk.cheetah.sitemap.Survey.Page.Issue.Input i) {
		InputField inputfieldModel = new InputField();

		if (i.getSize() != null)
			inputfieldModel.setSize(i.getSize());
		if (i.getType() != null)
			inputfieldModel.setType(i.getType());
		if (i.getMinlength() != null)
			inputfieldModel.setMinlength(i.getMinlength());
		if (i.getMaxlength() != null)
			inputfieldModel.setMaxlength(i.getMaxlength());
		if (i.getValue() != null)
			inputfieldModel.setValue(i.getValue());
		if (i.getMax() != null)
			inputfieldModel.setMax(i.getMax());
		if (i.getMin() != null)
			inputfieldModel.setMin(i.getMin());

		if (i.getOption() != null) {
			for (at.ac.uibk.cheetah.sitemap.Survey.Page.Issue.Input.Option o : i
					.getOption()) {
				OptionField optionfieldModel = createOptionfield(session, o);
				optionfieldModel.setInputField(inputfieldModel);
				session.save(optionfieldModel);
				inputfieldModel.getOptionFields().add(optionfieldModel);
			}
		}

		session.save(inputfieldModel);

		return inputfieldModel;
	}

	private OptionField createOptionfield(Session session,
			at.ac.uibk.cheetah.sitemap.Survey.Page.Issue.Input.Option o) {
		OptionField optionfieldModel = new OptionField();

		optionfieldModel.setText(o.getContent());
		optionfieldModel.setValue(o.getValue());

		return optionfieldModel;
	}

	private Config createConfig(Session session,
			at.ac.uibk.cheetah.sitemap.Survey.Config c) {
		Config config = new Config();

		config.setReleasedate(toDate(c.getReleasedate()));
		config.setExpiringdate(toDate(c.getExpiringdate()));

		if (c.getFormat() != null) {
			config.setFormat(c.getFormat());
		} else {
			config.setFormat("default");
		}
		if (c.getTemplate() != null) {
			config.setTemplate(c.getTemplate());
		} else {
			config.setTemplate("uibk.html");
		}

		if (c.getCooldown() != null) {
			config.setCooldown(c.getCooldown());
		} else {
			config.setCooldown(0);
		}

		session.save(config);

		return config;
	}

	private Survey createSurvey(Session session,
			at.ac.uibk.cheetah.sitemap.Survey s) {
		Survey survey = new Survey();

		Config configModel = createConfig(session, s.getConfig());

		survey.setConfig(configModel);
		survey.setTitle(s.getTitle());
        imgPath += s.getTitle() + "/";

		session.save(survey);

        // survey default is active
        survey.setActive(true);
		return survey;
	}

	private Issue createIssue(Session session,
			at.ac.uibk.cheetah.sitemap.Survey.Page.Issue i) {

		Issue issueModel = new Issue();
		issueModel.setQuestion(i.getQuestion());
		issueModel.setHint(i.getHint());
		issueModel.setDependency(i.getDependency());
		issueModel.setName(i.getName());
		if (i.getStyle() != null) {
			issueModel.setStyle(i.getStyle());
		} else {
			issueModel.setStyle("default");
		}

		if (i.isRequired() != null) {
			issueModel.setRequired(i.isRequired());
		} else {
			issueModel.setRequired(true);
		}

		if (i.getImg() != null) {
			Image imageModel = createImage(session, i.getImg());
			issueModel.setImage(imageModel);
		}

		InputField inputfieldModel = createInputfield(session, i.getInput());
		issueModel.setInputField(inputfieldModel);

		return issueModel;
	}

	private Page createPage(Session session,
			at.ac.uibk.cheetah.sitemap.Survey.Page p, Survey s) {
		Page pageModel = new Page();

		pageModel.setSurvey(s);
		if (p.getPagenumber() != null) {
			pageModel.setPagenumber(p.getPagenumber());
		} else {
			pageModel.setPagenumber(-1);
		}

		for (at.ac.uibk.cheetah.sitemap.Survey.Page.Issue i : p.getIssue()) {
			Issue issueModel = createIssue(session, i);
			issueModel.setPage(pageModel);
			session.save(issueModel);

			pageModel.getIssues().add(issueModel);
		}
		session.save(pageModel);
		return pageModel;
	}

	private Date toDate(XMLGregorianCalendar calendar) {
		if (calendar == null) {
			return null;
		}
		return new Date((calendar.toGregorianCalendar().getTime()).getTime());
	}

}
