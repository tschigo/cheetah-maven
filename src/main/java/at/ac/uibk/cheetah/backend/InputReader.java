package at.ac.uibk.cheetah.backend;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import at.ac.uibk.cheetah.hibernate.HibernateFactory;
import at.ac.uibk.cheetah.model.Answer;
import at.ac.uibk.cheetah.model.Issue;
import at.ac.uibk.cheetah.model.Page;
import at.ac.uibk.cheetah.model.PageTracking;
import at.ac.uibk.cheetah.model.Survey;
import at.ac.uibk.cheetah.model.Tracking;
import at.ac.uibk.cheetah.model.User;

/**
 * 
 * @author Patrick Ober and Michael Tscholl
 * @version 0.1
 * 
 */
public class InputReader {


	public void write2DB(Map<String, String[]> map, String surveyName, String sessionID) {

		Session session = HibernateFactory.create().openSession();
		session.beginTransaction();

		int pageID = Integer.parseInt(map.get("pageId")[0]);
		// get all issueIDs on the current page
		@SuppressWarnings("unchecked")
		List<Issue> issuesPerPage = session.createQuery(
				"FROM Issue i WHERE i.page.id = :pageID").setInteger("pageID", pageID).list();
				//"FROM Issue i WHERE i.page.id = '" + pageID + "'").list();

		PageTracking pageTracking = new PageTracking();
		User user = new User();

		// is only created once per survey per user
		if (map.get("page")[0].equals("1")) {
			// create tracking object
			Tracking tracking = createTracking(map);

			session.save(tracking);

			// create user object
			@SuppressWarnings("unchecked")
			List<Survey> surveys = (List<Survey>) session.createQuery(
					"from Survey where title = :surveyName").setString("surveyName", surveyName).list();
					//"from Survey where title ='" + surveyName + "'").list();
			user.setSurvey(surveys.get(0));
			user.setSessionID(sessionID);
			user.setTracking(tracking);

			user.setCreationDate(new Date());
			session.save(user);
		}

		for (Issue i : issuesPerPage) {

			if (map.get("q" + i.getId()) == null || map.get("q" + i.getId())[0].equals("")) {
				Answer answer = new Answer();
				answer.setIssue(i);
				answer.setAnswer(null);
				@SuppressWarnings("unchecked")
				List<User> users = (List<User>) session.createQuery(
						"from User where sessionID= :sessionID").setString("sessionID", sessionID).list();
						//"from User where sessionID='" + sessionID + "'").list();
				answer.setUser(users.get(users.size() - 1));
				session.save(answer);
			} else {

				for (String s : map.get("q"+ i.getId())) {
					Answer answer = new Answer();
					answer.setIssue(i);
					answer.setAnswer(s);

					@SuppressWarnings("unchecked")
					List<User> users = (List<User>) session.createQuery(
							"from User where sessionID= :sessionID").setString("sessionID", sessionID).list();
							//"from User where sessionID='" + sessionID + "'").list();
					answer.setUser(users.get(users.size() - 1));
					session.save(answer);
				}
			}
		}

		for (Map.Entry<String, String[]> m : map.entrySet()) {
			switch (m.getKey()) {
			case "mouse":
				if (m.getValue()[0].contains(",")) {
					pageTracking.setMousePos(m.getValue()[0].substring(0,
							m.getValue()[0].lastIndexOf(",")));
				}
				break;
			case "scroll":
				if (m.getValue()[0].contains(",")) {
					pageTracking.setScroll(m.getValue()[0].substring(0,
							m.getValue()[0].lastIndexOf(",")));
				}
				break;
			case "pageId":
				@SuppressWarnings("unchecked")
				List<Page> pages = (List<Page>) session.createQuery(
						"from Page where id=" + m.getValue()[0]).list();
				pageTracking.setPage(pages.get(0));
				break;
			case "time":
				pageTracking.setTime(Integer.parseInt(m.getValue()[0]));
				break;
			}
			if (m.getKey().charAt(0) == 'q') {
			}

		}
		// get user for each page for tracking issues
		@SuppressWarnings("unchecked")
		List<User> pageTrackingUser = (List<User>) session.createQuery(
				"from User where sessionID= :sessionID").setString("sessionID", sessionID).list();
				//"from User where sessionID='" + sessionID + "'").list();
		pageTracking.setUser(pageTrackingUser.get(pageTrackingUser.size()-1));
		session.save(pageTracking);

		// commit and close
		session.getTransaction().commit();
		session.close();
		HibernateFactory.close();
	}

	private Tracking createTracking(Map<String, String[]> map) {
		Tracking tracking = new Tracking();
		for (Map.Entry<String, String[]> m : map.entrySet()) {
			switch (m.getKey()) {
			case "ip":
				tracking.setIp(m.getValue()[0]);
				break;
			case "browser":
				tracking.setBrowser(m.getValue()[0].substring(0,
						m.getValue()[0].indexOf(" on ")));
				tracking.setOs(m.getValue()[0].substring(m.getValue()[0]
						.indexOf(" on ") + 4));
				break;
			case "screen":
				tracking.setResolution(m.getValue()[0]);
				break;
			}
		}
		return tracking;
	}

}
