package at.ac.uibk.cheetah.backend;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import at.ac.uibk.cheetah.hibernate.HibernateFactory;
import at.ac.uibk.cheetah.model.Answer;
import at.ac.uibk.cheetah.model.User;

/**
 * 
 * @author Patrick Ober and Michael Tscholl
 * @version 0.1
 * 
 */
public class ExportSurvey {
	private final String PATH = "/tmp/";
	private final String SEPARATOR = ";";
	private final String MULTISEPARATOR = ",";

	/**
	 * Question Answer Export
	 * 
	 * @param surveyName
	 */
	public void createCSVqa(String surveyName) {
		Session session = HibernateFactory.create().openSession();
		session.beginTransaction();

		// questions query
		@SuppressWarnings("unchecked")
		List<String[]> questionContext = session.createQuery(
				"SELECT i.id, i.question FROM Issue i" + " JOIN i.page p"
						+ " JOIN p.survey s" 
						+ " WHERE s.title = :surveyName").setString("surveyName", surveyName).list();
						//+ " WHERE s.title = '" + surveyName + "'").list();

		String questions = "";
		for (Object[] q : questionContext) {
			questions += q[1] + SEPARATOR;
		}

		// csv context
		String head = "title" + SEPARATOR + "sessionID" + SEPARATOR + questions;
		List<String> context = new ArrayList<String>();
		context.add(head);

		// get all users for that survey
		@SuppressWarnings("unchecked")
		List<Object[]> userContext = session.createQuery(
				"FROM User u" + " JOIN u.survey s" 
				+ " WHERE s.title = :surveyName").setString("surveyName", surveyName).list();
				//+ " WHERE s.title = '" + surveyName + "'").list();

		for (Object[] u : userContext) {

			// get all answers per user
			@SuppressWarnings("unchecked")
			List<Object[]> answerContext = session.createQuery(
					" FROM Answer a JOIN a.user u JOIN u.survey s"
					+ " WHERE s.title = :surveyName"	
					//+ " WHERE s.title = '" + surveyName
					+ " and u.id = " + ((User) u[0]).getId()).setString("surveyName", surveyName).list();

			String answers = surveyName + SEPARATOR
					+ ((User) u[0]).getSessionID() + SEPARATOR;

			for (int i = 0; i < answerContext.size(); i++) {

				if (i + 1 < answerContext.size()
						&& ((Answer) answerContext.get(i)[0]).getIssue()
								.getId() == ((Answer) answerContext.get(i + 1)[0])
								.getIssue().getId()) {
					answers += ((Answer) answerContext.get(i)[0]).getAnswer()
							+ MULTISEPARATOR;

				} else {

					if (((Answer) answerContext.get(i)[0]).getAnswer() != null) {
						answers += ((Answer) answerContext.get(i)[0])
								.getAnswer() + SEPARATOR;
					} else {
						answers += SEPARATOR;
					}
				}
			}
			context.add(answers);
		}

		session.close();
		HibernateFactory.close();

		try {
			FileWriter writer = new FileWriter(PATH + surveyName
					+ "_answers.csv");

			for (String s : context) {
				writer.append(s + "\n");
			}

			writer.flush();
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Export with usertracking data
	 * 
	 * @param surveyName
	 *            in db as survey title
	 */
	public void createCSVtracking(String surveyName) {
		Session session = HibernateFactory.create().openSession();
		session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<String[]> context = session
				.createQuery(
						"SELECT s.title, u.sessionID, u.creationDate, t.os, t.browser, t.ip, t.resolution, sum(pt.time) from User u"
								+ " JOIN u.survey s "
								+ " JOIN u.tracking t "
								+ " JOIN u.pageTracking pt"
								+ " WHERE s.title = :surveyName"
								+ " group by u.id").setString("surveyName", surveyName).list();
		context.add(0, new String[] { "title", "sessionID", "creationDate",
				"OS", "Browser", "IP", "Resolution", "Total Time [ms]" });

		session.close();
		HibernateFactory.close();

		try {
			FileWriter writer = new FileWriter(PATH + surveyName
					+ "_userTracking.csv");

			for (Object[] o : context) {
				String trackingdata = o[0] + SEPARATOR + o[1] + SEPARATOR
						+ o[2] + SEPARATOR + o[3] + SEPARATOR + o[4]
						+ SEPARATOR + o[5] + SEPARATOR + o[6] + SEPARATOR
						+ o[7] + "\n";
				trackingdata = trackingdata.replace((SEPARATOR + "null" + SEPARATOR), (SEPARATOR
						+ "" + SEPARATOR));
				writer.append(trackingdata);
			}

			writer.flush();
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Export with tracked page data
	 * 
	 * @param surveyName
	 */
	public void createCSVpageTracking(String surveyName) {
		Session session = HibernateFactory.create().openSession();
		session.beginTransaction();

		@SuppressWarnings("unchecked")
		List<String[]> context = session
				.createQuery(
						"select s.title, u.sessionID, u.creationDate, pt.mousePos, pt.scroll, pt.time from User u"
								+ " join u.survey s "
								+ " join u.pageTracking pt "
								+ " where s.title = :surveyName").setString("surveyName", surveyName)
								//+ " where s.title = '" + surveyName + "'")
				.list();
		context.add(0, new String[] { "title", "sessionID", "creationDate",
				"Mouse Data", "Scroll Data", "Time/Page [ms]" });

		session.close();
		HibernateFactory.close();

		try {
			FileWriter writer = new FileWriter(PATH + surveyName
					+ "_pageTracking.csv");

			for (Object[] o : context) {
				String trackingdata = o[0] + SEPARATOR + o[1] + SEPARATOR
						+ o[2] + SEPARATOR + o[3] + SEPARATOR + o[4]
						+ SEPARATOR + o[5] + "\n";
				trackingdata = trackingdata.replace((SEPARATOR + "null" + SEPARATOR), (SEPARATOR
						+ "" + SEPARATOR));
				writer.append(trackingdata);
			}

			writer.flush();
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
